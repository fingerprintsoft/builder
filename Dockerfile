FROM fingerprintsoft/maven
MAINTAINER Fuzail Sarang <fuzail@fingerprintsoft.co>

EXPOSE 3306

ENV PJS_HOME=/usr/lib/phantomjs \
    TERM=xterm

COPY mariadb_init.sh /mariadb_init.sh
COPY my.cnf /etc/mysql/my.cnf

RUN apk add --no-cache mariadb mariadb-client wget bash curl && \
  cd /tmp && \
  curl -Ls "https://github.com/dustinblackman/phantomized/releases/download/2.1.1/dockerized-phantomjs.tar.gz" | tar xz -C / && \
  wget "https://cnpmjs.org/mirrors/phantomjs/phantomjs-2.1.1-linux-x86_64.tar.bz2" && \
  tar -xvf phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
  mv "phantomjs-2.1.1-linux-x86_64" "$PJS_HOME" && \
  ln -s "$PJS_HOME/bin/phantomjs" /usr/bin/phantomjs  && \
  apk del wget curl && \
  rm -rf /tmp/* /var/cache/apk/* && \
  sh /mariadb_init.sh

ENTRYPOINT ["/usr/bin/mysqld_safe", "--datadir=/var/lib/mysql"]

