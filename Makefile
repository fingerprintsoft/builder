TAG ?= mvn3.3_jdk8_mysql
.PHONY: all

all: build push

build:
	docker build --rm -t fingerprintsoft/builder .
	docker tag fingerprintsoft/builder fingerprintsoft/builder:${TAG}

push:
	docker push fingerprintsoft/builder
	docker push fingerprintsoft/builder:${TAG}

