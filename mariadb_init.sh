#!/bin/ash

/usr/bin/mysql_install_db --user=root
/usr/bin/mysqld_safe --datadir="/var/lib/mysql" > /dev/null 2>&1 &

mysqladmin --silent --wait=30 ping

mysql -uroot -e "CREATE USER 'build'@'localhost' IDENTIFIED BY 'buildmeplease'"
mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'build'@'localhost' WITH GRANT OPTION"
mysql -uroot -e "FLUSH PRIVILEGES"

echo "=> Done!"

echo "========================================================================"
echo "You can now connect to this MariaDB Server using:"
echo ""
echo "    mysql -ubuild -pbuildmeplease -h<host> -P<port>"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "MariaDB user 'root' has no password but only allows local connections"
echo "========================================================================"

mysqladmin -uroot shutdown
